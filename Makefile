all: build

build:
	docker build -t blog:0.1 .

run: build
	docker run --rm -d --name blog -p 80:8000 -v db:/code/db blog:0.1

stop:
	docker stop blog